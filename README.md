# Microservices with Spring Boot and Spring Cloud

## Part I - theory

### What is a microservice
A single deployable unit of business functionality, independent of other services in terms of developing, testing, deploying and maintaining.

### Why microservices
#### Addressed problems
* monolith hell
  * steep learning curve for new-comers
  * slower IDE
  * longer build times and test execution
* scaling
  * different modules require different machine setup (memory, cpu, bandwidth)
* debugging
  * how to isolate one part of the application
* locked into an increasingly obsolete technology stack
* long time to market

### Design challenges
* isolating candidates for decomposing a monolith
* transactions spanning multiple micro-services (Saga pattern)
* retrieving data from multiple micro-services (API composition, CQRS)
* service orchestration (Kubernetes)
* when to start with micro-services
* how to keep consistency in distributed systems (share nothing principle)
* lot of components to maintain (code repository, CI/CD, deployment, monitoring, logging)

### Technical challenges
* difficult to debug a distributed system (tracing)
* distributed systems have to find each other (service discovery/registry)
* cascading failures (circuit breaker)
* operational and business metrics (monitoring)
* gather log messages in one place (log aggregators, like Splunk, ELK)
* expose unified contact point to end-user (API Gateway)
  * protocol translation
  * request routing
  * API composition
  * edge functions
    * authentication / authorization
    * rate limiting
    * caching
    * metric collection of API usage for analytics and billing purposes
    * request logging
    * A/B testing

### Defining service APIs
* [API first approach](https://confluence.build.ingka.ikea.com/pages/viewpage.action?spaceKey=INTERINT&title=API+first+approach)

### Synchronous communication patterns (REST vs. gRPC)
* benefits of REST
  * familiar
  * easy testing (web browser, Postman, curl)
  * HTTP is firewall friendly

* benefits of gRPC
  * API design based on procedures, rather than on HTTP methods
  * efficient and compact exchange
  * bidirectional streaming (request-reply + messaging pattern)

* reply or timeout (deterministic)
* no intermediate broker required

### Asynchronous communication
* fire and forget
* asynchronous replies
* message driven, non-blocking
* challenges
  * transactional messaging
  * duplication of messages
  * message order

### And more... aka further reading
[Microservices patterns](https://microservices.io/patterns/index.html)

## Part II - Spring Cloud components for building microservices

[Spring Cloud](https://spring.io/projects/spring-cloud) provides tools for developers to quickly build some of the 
common patterns in distributed systems (e.g. configuration management, service discovery, circuit breakers, 
intelligent routing, micro-proxy, control bus, one-time tokens, global locks, leadership election, distributed sessions, 
cluster state).

[Spring Cloud Netflix](https://spring.io/projects/spring-cloud-netflix) Spring Cloud Netflix provides Netflix OSS 
integrations for Spring Boot apps through autoconfiguration and binding to the Spring Environment and other Spring 
programming model idioms. The patterns provided include Service Discovery (Eureka), Circuit Breaker (Hystrix), 
Intelligent Routing (Zuul) and Client Side Load Balancing (Ribbon).

[Compatibility Matrix](https://spring.io/projects/spring-cloud)

### Microservices architecture
![](doc/architecture.png)

* Service Discovery Server - Eureka (discovery-service) 
  * http://localhost:8761/

* Service Discovery Client - Eureka (furniture-service) 
  * http://localhost:8088/furniture

* Synchronous service communication via http - ```@RestController``` (analytics-service)
  * http://localhost:8089/top-furniture
  * Feign Client vs RestTemplate
    * eureka discovery
    * ribbon (weighted load balancing rule) https://dzone.com/articles/microservices-in-spring-eureka

* Circuit Breaker - Hystrix (analytics-service) 
  * http://localhost:8089/hystrix/monitor?stream=http%3A%2F%2Flocalhost%3A8089%2Factuator%2Fhystrix.stream

* Metrics/Health check - Actuator (analytics-service) 
  * http://localhost:8089/actuator/metrics/jvm.memory.max
  * https://docs.spring.io/spring-boot/docs/current/reference/html/production-ready-features.html#production-ready-endpoints-exposing-endpoints

* API Gateway - Zuul (api-gateway)
  * http://localhost:8080/furniture/furniture
  * http://localhost:8080/analytics/top-furniture
  * http://localhost:8080/discovery/

* Tracing - Sleuth, Zipkin (all services)
  * ```docker-compose up -d```
  * http://localhost:9411/zipkin/

## Assignment
![](doc/assignment.png)
* Expose an http `GET` endpoint via ```@RestController``` in finance-service returning salary for given employee level and time in company:

| level  | time | salary |
|--------|------|--------|
| junior | 1yr  | 100    |
| junior | 2yr  | 200    |
| mid    | 1yr  | 300    |
| mid    | 2yr  | 400    |
| mid    | 3yr  | 500    |
| senior | 1yr  | 600    |
| senior | 2yr  | 700    |

Use ```@RequestParam``` to retrieve query parameters. For example `http://finance-service/salaries?level=junior&time=2yr`
should return:
```
{
    salary: 200
}
```

* Expose an http `GET` endpoint via ```@RequestController``` in employee-service returning employee data (name, level, salary) by name.

* Hide the employee-service and hr-service APIs behind an API gateway.

* Setup tracing, run a query via the API Gateway and verify the traces in Zipkin.

* Create a method handling `POST` request to add employees in the hr-service
