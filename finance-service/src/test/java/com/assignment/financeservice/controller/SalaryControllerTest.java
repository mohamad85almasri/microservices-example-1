package com.assignment.financeservice.controller;

import com.assignment.financeservice.service.SalaryLevelService;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@WebMvcTest
public class SalaryControllerTest {
    
    @Autowired
    MockMvc mockMvc;

    @MockBean
    SalaryLevelService salaryService;
    
    @Test
    public void returnSalary_test() throws Exception {
        
        Mockito.when(salaryService.getSalaryByLevelAndTime("Junior", 1)).thenReturn(100);
        Mockito.when(salaryService.getSalaryByLevelAndTime("Mid", 1)).thenReturn(300);
        Mockito.when(salaryService.getSalaryByLevelAndTime("Senior", 1)).thenReturn(600);
        
        mockMvc.perform(MockMvcRequestBuilders.get("/salaries?level=Junior&time=1"))
                .andExpect(MockMvcResultMatchers.status().isOk());
                //.andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_VALUE))
                //.andExpect(MockMvcResultMatchers.content().string("100"));
    }
}
