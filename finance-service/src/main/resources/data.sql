INSERT INTO SalaryLevel (employee_level, employee_time, salary) VALUES ('Junior', 1, 100);
INSERT INTO SalaryLevel (employee_level, employee_time, salary) VALUES ('Junior', 2, 200);
INSERT INTO SalaryLevel (employee_level, employee_time, salary) VALUES ('Mid', 1, 300);
INSERT INTO SalaryLevel (employee_level, employee_time, salary) VALUES ('Mid', 2, 400);
INSERT INTO SalaryLevel (employee_level, employee_time, salary) VALUES ('Mid', 3, 500);
INSERT INTO SalaryLevel (employee_level, employee_time, salary) VALUES ('Senior', 1, 600);
INSERT INTO SalaryLevel (employee_level, employee_time, salary) VALUES ('Senior', 2, 700);