package com.assignment.financeservice.dao;

import com.assignment.financeservice.model.SalaryLevel;
import com.assignment.financeservice.model.SalaryLevelPrimaryKey;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(path = "salary")
public interface SalaryLevelRepository extends CrudRepository<SalaryLevel, SalaryLevelPrimaryKey> {
    
    
}
