package com.assignment.financeservice.model;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@Getter
public class SalaryLevelPrimaryKey implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private String level;

    private int time;
    
}
