package com.assignment.financeservice.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "SALARYLEVEL")
@IdClass(SalaryLevelPrimaryKey.class)
public class SalaryLevel {

    @Id
    @Column(name = "employee_level")
    private String level;

    @Id
    @Column(name = "employee_time")
    private int time;

    @Column(name = "salary")
    private int salary;
    
}
