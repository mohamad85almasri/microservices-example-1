package com.assignment.financeservice.service;

public interface SalaryLevelService {

    public int getSalaryByLevelAndTime(String level, int time);
    
}
