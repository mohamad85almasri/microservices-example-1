package com.assignment.financeservice.service.impl;

import java.util.Optional;

import com.assignment.financeservice.dao.SalaryLevelRepository;
import com.assignment.financeservice.model.SalaryLevel;
import com.assignment.financeservice.model.SalaryLevelPrimaryKey;
import com.assignment.financeservice.service.SalaryLevelService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component
public class SalaryLevelServiceimpl implements SalaryLevelService {

    private final SalaryLevelRepository salaryLevelRepository;
    
    @Autowired
    public SalaryLevelServiceimpl(SalaryLevelRepository salaryLevelRepository) {
        this.salaryLevelRepository = salaryLevelRepository;
    }
    
    @Override
    public int getSalaryByLevelAndTime(String level, int time) {
        Optional<SalaryLevel> op = salaryLevelRepository.findById(new SalaryLevelPrimaryKey(level, time));
        if(op.isPresent()) {
            return op.get().getSalary();
        }
        return -1;
    }
}
