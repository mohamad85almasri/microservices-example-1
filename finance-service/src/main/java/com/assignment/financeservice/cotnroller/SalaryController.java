package com.assignment.financeservice.cotnroller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;



import com.assignment.financeservice.service.SalaryLevelService;



@RestController
public class SalaryController {

    private final SalaryLevelService salaryLevelService;

    @Autowired
    public SalaryController(SalaryLevelService salaryLevelService) {
		this.salaryLevelService = salaryLevelService;
	}

    @GetMapping("/salaries")
    public int getEmployeeById(@RequestParam("level") String level, @RequestParam("time") int time) {
        return salaryLevelService.getSalaryByLevelAndTime(level, time);
    }

	
    
}
