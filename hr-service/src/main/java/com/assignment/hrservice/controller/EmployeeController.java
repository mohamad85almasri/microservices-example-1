package com.assignment.hrservice.controller;

import java.net.URI;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.*;

import com.assignment.hrservice.model.Employee;
import com.assignment.hrservice.dto.EmployeeDTO;
import com.assignment.hrservice.dto.EmployeeInfo;
import com.assignment.hrservice.service.EmployeeService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
public class EmployeeController {

    private EmployeeService employeeService;

    @Autowired
    public EmployeeController(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    @GetMapping("/employee")
    public HttpEntity<EmployeeInfo> getEmployeeInfo(@RequestParam("id") Integer id) {
        log.info("Returning Employee Information");
        EmployeeInfo employeeInfo = employeeService.getEmployee(id);
        employeeInfo.add(linkTo(methodOn(EmployeeController.class).getEmployeeInfo(id)).withSelfRel());

        return new ResponseEntity<>(employeeInfo, HttpStatus.OK);
    }

    

    @PostMapping("/employee")
    public ResponseEntity<Employee> newEmployee(@RequestBody EmployeeDTO employee) {
        Employee savedEmployee = employeeService.save(employee);
        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
			.buildAndExpand(savedEmployee.getEmployeeId()).toUri();
        
        return ResponseEntity.created(location).build();
    }
}
