package com.assignment.hrservice.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Employee {

    @Id
    @Column(name = "employee_id")
    private int employeeId;

    @Column(name = "employee_first_name")
    private String firstName;

    @Column(name = "employee_last_name")
    private String lastName;

    @Column(name = "title")
    private String employeeRole;

    @Column(name = "employee_level")
    private String employeeLevel;

    @Column(name = "team")
    private String team;

    @Column(name = "year_number")
    private int employeeYear;

}
