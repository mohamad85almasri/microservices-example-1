package com.assignment.hrservice.dto;

import org.springframework.hateoas.RepresentationModel;


import lombok.Data;
import lombok.EqualsAndHashCode;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

@Data
@EqualsAndHashCode(callSuper = false)
public class EmployeeInfo extends RepresentationModel<EmployeeInfo>{
    
    private String firstName;

    private String lastName;

    private String employeeLevel;

    private int employeeYear;

    private int salary;

    @JsonCreator
    public EmployeeInfo(@JsonProperty("firstName") String firstName, @JsonProperty("lastName") String lastName, 
        @JsonProperty("employeeLevel")String employeeLevel, @JsonProperty("employeeYear") int employeeYear, 
        @JsonProperty("salary") int salary) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.employeeLevel = employeeLevel;
        this.employeeYear = employeeYear;
        this.salary = salary;
    }
}
