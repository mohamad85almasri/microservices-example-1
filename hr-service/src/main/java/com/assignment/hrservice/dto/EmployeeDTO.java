package com.assignment.hrservice.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class EmployeeDTO {

    private int employeeId;

    private String firstName;

    private String lastName;

    private String employeeRole;

    private String employeeLevel;

    private String team;

    private int employeeYear;
}