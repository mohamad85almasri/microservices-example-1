package com.assignment.hrservice.dto;

import lombok.Data;

@Data
public class SalaryLevelPOJO {

    private String level;

    private int time;

    private int salary;
    
}
