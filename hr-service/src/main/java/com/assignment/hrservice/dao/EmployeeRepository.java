package com.assignment.hrservice.dao;

import com.assignment.hrservice.model.Employee;

import org.springframework.data.jpa.repository.JpaRepository;


public interface EmployeeRepository extends JpaRepository<Employee, Integer> {
    
}
