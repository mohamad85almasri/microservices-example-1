package com.assignment.hrservice.service;

import java.util.List;

import com.assignment.hrservice.model.Employee;
import com.assignment.hrservice.dto.EmployeeDTO;
import com.assignment.hrservice.dto.EmployeeInfo;

public interface EmployeeService {
    
    public EmployeeInfo getEmployee(Integer id);

    public List<Employee> getAllEmployee();

    public Employee save(EmployeeDTO employee);
}
