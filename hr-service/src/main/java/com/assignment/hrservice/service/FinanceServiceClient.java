package com.assignment.hrservice.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;


@FeignClient("finance-service")
public interface FinanceServiceClient {
    
    @GetMapping("/salaries")
    public int readSalary(@RequestParam("level") String level, @RequestParam("time") int time);

}
