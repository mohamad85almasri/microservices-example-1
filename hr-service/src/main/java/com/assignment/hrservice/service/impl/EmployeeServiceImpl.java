package com.assignment.hrservice.service.impl;

import java.util.List;
import java.util.Optional;

import com.assignment.hrservice.dao.EmployeeRepository;
import com.assignment.hrservice.model.Employee;
import com.assignment.hrservice.dto.EmployeeDTO;
import com.assignment.hrservice.dto.EmployeeInfo;
import com.assignment.hrservice.service.EmployeeService;
import com.assignment.hrservice.service.FinanceServiceClient;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class EmployeeServiceImpl implements EmployeeService {

    private EmployeeRepository employeeRepository;

    private FinanceServiceClient financeServiceClient;

    @Autowired
    public EmployeeServiceImpl(EmployeeRepository employeeRepository, FinanceServiceClient financeServiceClient) {
        this.employeeRepository = employeeRepository;
        this.financeServiceClient = financeServiceClient;
    }

    @Override
    @HystrixCommand(fallbackMethod = "fallback")
    public EmployeeInfo getEmployee(Integer id) {
        Optional<Employee> op = employeeRepository.findById(id);
        if (op.isPresent()) {
            Employee employee = op.get();
            int salary = financeServiceClient.readSalary(employee.getEmployeeLevel(), employee.getEmployeeYear());
            
            return new EmployeeInfo(employee.getFirstName(), employee.getLastName(),
                employee.getEmployeeLevel(), employee.getEmployeeYear(), salary);

        }
        return null;
    }

    @Override
    public List<Employee> getAllEmployee() {
        return employeeRepository.findAll();
    }

    @Override
    public Employee save(EmployeeDTO employeeDTO) {
        //convert dto to entity
        Employee employee = new Employee(employeeDTO.getEmployeeId(), employeeDTO.getFirstName(), employeeDTO.getLastName(), employeeDTO.getEmployeeRole(), employeeDTO.getEmployeeLevel(), employeeDTO.getTeam(), employeeDTO.getEmployeeYear());
        return employeeRepository.save(employee);
    }

    public  EmployeeInfo fallback(Integer id) {
        return new EmployeeInfo("Error", "Error2", "Error", 0, 0);
    }
}
