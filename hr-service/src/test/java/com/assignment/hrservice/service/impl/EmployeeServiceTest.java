package com.assignment.hrservice.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.assignment.hrservice.dao.EmployeeRepository;
import com.assignment.hrservice.model.Employee;
import com.assignment.hrservice.dto.EmployeeDTO;
import com.assignment.hrservice.dto.EmployeeInfo;
import com.assignment.hrservice.service.EmployeeService;
import com.assignment.hrservice.service.FinanceServiceClient;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;


import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
public class EmployeeServiceTest {

    @MockBean
    EmployeeRepository employeeRepository;

    @Autowired
    EmployeeService employeeServiceImpl;

    @MockBean
    FinanceServiceClient financeServiceClient;

    private List<Employee> repository;

    @BeforeEach
    public void setup() {
        repository = new ArrayList<>();
        repository.add(new Employee(1, "Mo", "A", "Tester", "Junior", "Team1", 1));
        repository.add(new Employee(2, "Karamat", "M", "Developer", "Senior", "Team2", 2));
        repository.add(new Employee(3, "Luan", "M", "Developer", "Junior", "Team3", 1));
    }

    @Test
    public void getEmployeeInfo_Test() {
        Mockito.when(employeeRepository.findById(1)).thenReturn(Optional.of(repository.get(0)));
        Mockito.when(employeeRepository.findById(2)).thenReturn(Optional.of(repository.get(1)));
        Mockito.when(employeeRepository.findById(3)).thenReturn(Optional.of(repository.get(2)));

        Mockito.when(financeServiceClient.readSalary("Junior", 1)).thenReturn(100);
        Mockito.when(financeServiceClient.readSalary("Senior", 2)).thenReturn(700);


        assertEquals(new EmployeeInfo("Mo", "A","Junior", 1, 100), employeeServiceImpl.getEmployee(1));
        assertEquals(new EmployeeInfo("Karamat", "M", "Senior", 2, 700), employeeServiceImpl.getEmployee(2));
        assertEquals(new EmployeeInfo("Luan", "M", "Junior", 1, 100), employeeServiceImpl.getEmployee(3));
    }

    @Test
    public void save_Test() {
        Mockito.when(employeeRepository.save(repository.get(0))).thenReturn(repository.get(0));
        Mockito.when(employeeRepository.save(repository.get(1))).thenReturn(repository.get(1));
        Mockito.when(employeeRepository.save(repository.get(2))).thenReturn(repository.get(2));   
        
        
        assertEquals(new Employee(1, "Mo", "A", "Tester", "Junior", "Team1", 1), employeeServiceImpl.save(new EmployeeDTO(1, "Mo", "A", "Tester", "Junior", "Team1", 1)));
        assertEquals(new Employee(2, "Karamat", "M", "Developer", "Senior", "Team2", 2), employeeServiceImpl.save(new EmployeeDTO(2, "Karamat", "M", "Developer", "Senior", "Team2", 2)));
        assertEquals(new Employee(3, "Luan", "M", "Developer", "Junior", "Team3", 1), employeeServiceImpl.save(new EmployeeDTO(3, "Luan", "M", "Developer", "Junior", "Team3", 1)));
    }

    
}
