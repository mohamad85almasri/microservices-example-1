package com.assignment.hrservice.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.assignment.hrservice.dao.EmployeeRepository;
import com.assignment.hrservice.dto.EmployeeDTO;
import com.assignment.hrservice.model.Employee;
import com.assignment.hrservice.service.EmployeeService;
import com.assignment.hrservice.service.FinanceServiceClient;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.http.MediaType;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;


import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

import static org.hamcrest.Matchers.*;



@WebMvcTest
public class EmplyeeControllerTest {
    
    @Autowired
    MockMvc mockMvc;

    @MockBean
    EmployeeService employeeService;

    @MockBean
    FinanceServiceClient financeServiceClient;

    @MockBean
    EmployeeRepository employeeRepository;

    private List<Employee> repository;
    

    @BeforeEach
    public void setup() {
        repository = new ArrayList<>();
        repository.add(new Employee(1, "Mo", "A", "Tester", "Junior", "Team1", 1));
        repository.add(new Employee(2, "Karamat", "M", "Developer", "Senior", "Team2", 2));
        repository.add(new Employee(3, "Luan", "M", "Developer", "Junior", "Team3", 1));
    }

    @Test
    public void getEmployeeInfo_Test() throws Exception {
        Mockito.when(employeeRepository.findById(1)).thenReturn(Optional.of(repository.get(0)));
        Mockito.when(employeeRepository.findById(2)).thenReturn(Optional.of(repository.get(1)));
        Mockito.when(employeeRepository.findById(3)).thenReturn(Optional.of(repository.get(2)));

        Mockito.when(financeServiceClient.readSalary("Junior", 1)).thenReturn(100);
        Mockito.when(financeServiceClient.readSalary("Senior", 2)).thenReturn(700);
        
        mockMvc.perform(get("/employee?id=1"))
            .andDo(print())
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.salary", is(100)));

        mockMvc.perform(get("/employee?id=2"))
            .andDo(print())
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.salary", is(700)));

        mockMvc.perform(get("/employee?id=3"))
            .andDo(print())
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.salary", is(100)));
    }

    @Test
    public void newEmployee_Test() throws Exception {
        EmployeeDTO stef = new EmployeeDTO(4, "Stefan", "V", "Developer", "Junior", "Team4", 1);
        Mockito.when(employeeService.save(stef)).thenReturn(new Employee(4, "Stefan", "V", "Developer", "Junior", "Team4", 1));

        mockMvc.perform(post("/employee")
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .content(new ObjectMapper().writeValueAsString(new Employee(4, "Stefan", "V", "Developer", "Junior", "Team4", 1)))
            .accept(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(status().isCreated());
    }
  
}
    
