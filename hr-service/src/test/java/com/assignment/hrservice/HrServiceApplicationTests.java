package com.assignment.hrservice;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.assignment.hrservice.controller.EmployeeController;
import com.assignment.hrservice.dao.EmployeeRepository;
import com.assignment.hrservice.dto.EmployeeDTO;
import com.assignment.hrservice.dto.EmployeeInfo;
import com.assignment.hrservice.model.Employee;
import com.assignment.hrservice.service.FinanceServiceClient;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.*;

import java.net.URI;

@SpringBootTest
class HrServiceApplicationTests {

	@Autowired
	private EmployeeRepository employeeRepository;

	@Autowired
	private EmployeeController employeeController;

	@MockBean
    private FinanceServiceClient financeServiceClient;

	@BeforeEach
	public void setup() {
		employeeRepository.save(new Employee(4, "Stefan", "V", "Tester", "Junior", "Team1", 1));
		employeeRepository.save(new Employee(5, "Alfredo", "F", "Developer", "Senior", "Team3", 2));
	}

	@Test
    public void getEmployeeInfo_Test() {
        Mockito.when(financeServiceClient.readSalary("Junior", 1)).thenReturn(100);
		Mockito.when(financeServiceClient.readSalary("Senior", 2)).thenReturn(700);
		
		EmployeeInfo stefan = new EmployeeInfo("Stefan", "V", "Junior", 1, 100);
		stefan.add(linkTo(methodOn(EmployeeController.class).getEmployeeInfo(4)).withSelfRel());

		EmployeeInfo alfredo = new EmployeeInfo("Alfredo", "F", "Senior", 2, 700);
		alfredo.add(linkTo(methodOn(EmployeeController.class).getEmployeeInfo(5)).withSelfRel());

		EmployeeInfo karamat = new EmployeeInfo("Karamat", "M", "Senior", 2, 700);
		karamat.add(linkTo(methodOn(EmployeeController.class).getEmployeeInfo(2)).withSelfRel());

        assertEquals(new ResponseEntity<>(stefan, HttpStatus.OK), employeeController.getEmployeeInfo(4));
        assertEquals(new ResponseEntity<>(alfredo, HttpStatus.OK), employeeController.getEmployeeInfo(5));
        assertEquals(new ResponseEntity<>(karamat, HttpStatus.OK), employeeController.getEmployeeInfo(2));
	}
	
	@Test
    public void save_Test() {

		Employee tommy = new Employee(6, "Tommy", "R", "Tester", "Junior", "Team1", 1);
		URI tommyPath = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(tommy.getEmployeeId()).toUri();
		Employee philip = new Employee(7, "Philip", "V", "Developer", "Junior", "Team3", 1);
		URI philipPath = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(philip.getEmployeeId()).toUri();
        
        
        assertEquals(ResponseEntity.created(tommyPath).build(), employeeController.newEmployee(new EmployeeDTO(6, "Tommy", "R", "Tester", "Junior", "Team1", 1)));
        assertEquals(ResponseEntity.created(philipPath).build(), employeeController.newEmployee(new EmployeeDTO(7, "Philip", "V", "Developer", "Junior", "Team3", 1)));
    }
	

}
